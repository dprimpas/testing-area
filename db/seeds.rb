Product.delete_all
Product.create! id: 1, name: "American Spruce", price: 60.49, active: true
Product.create! id: 2, name: "Delux tree stand", price: 10.29, active: true
Product.create! id: 3, name: "Holly Wreath", price: 8.99, active: true

OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped"
OrderStatus.create! id: 4, name: "Cancelled"